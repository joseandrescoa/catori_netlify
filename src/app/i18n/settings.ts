export const fallbackLng = "en";
export const languages = [fallbackLng, "pt"];
export const defaultNS = "translation";

export function getOptions(
  lang = fallbackLng,
  ns: string | string[] = defaultNS
) {
  return {
    // debug: true,
    supportedLngs: languages,
    fallbackLng,
    lang,
    fallbackNS: defaultNS,
    defaultNS,
    ns,
  };
}
