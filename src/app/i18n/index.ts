import { createInstance } from 'i18next'
import { initReactI18next } from 'react-i18next/initReactI18next'
import { getOptions } from './settings'
import resourcesToBackend from 'i18next-resources-to-backend'

const initI18next = async (lng: string, ns?: string | string[]) => {
  const i18nInstance = createInstance()
  await i18nInstance
    .use(initReactI18next)
    // @ts-ignore
    .use(resourcesToBackend((_, __) => import(`./locales/${lng}.json`)))
    .init(getOptions(lng, ns))
  return i18nInstance
}

export async function useTranslation (lng: string, ns = undefined, options: { keyPrefix?: string } = {}) {
  const i18nextInstance = await initI18next(lng, ns)
  // TODO you can pass this to client components
  // console.log(i18nextInstance.store.data)
  return {
    t: i18nextInstance.getFixedT(lng, Array.isArray(ns) ? ns[0] : ns, options.keyPrefix),
    i18n: i18nextInstance
  }
}