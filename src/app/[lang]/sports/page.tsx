import UnderConstruction from "@/components/underConstruction/component";
import SportsComponent from "@/components/sportspage/component";
import HomeComponent from "@/components/homepage/component";

export default async function Sports() {


  return (
      <div className="flex flex-col items-center">
        <SportsComponent />
      </div>
  );
}
