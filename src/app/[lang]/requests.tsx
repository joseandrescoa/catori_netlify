type CasinoGame = {
  title: string;
  provider: string;
  bg_image: any;
};

type CasinoGames = {
  all: CasinoGame[];
  filtered: CasinoGame[];
};

// List casino games on homepage
/*
export const getCasinoGames = async (lang?: string, searchTerm?: string) => {
  const responseAll = await fetch(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/casino-games?locale=${lang}&populate=*`
  );

  const responseFiltered = await fetch(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/casino-games?locale=${lang}&populate=*&filters[title][$contains]=${searchTerm}`
  );

  let data: CasinoGames = {
    all: await responseAll.json(),
    filtered: await responseFiltered.json(),
  };
  return data;
};
*/
//List promotions on home header
export const getPromotionData = async (lang?: string) => {
  const response = await fetch(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/promotions?locale=${lang}&populate=*`
  );

  let data = response.json();
  return data;
};

//List registration fields on register pop-up
export const getRegisterForms = async (lang?: string) => {
  const response = await fetch(
    `${process.env.NEXT_PUBLIC_STRAPI_URL}/register-forms?locale=${lang}`
  );

  const data = response.json();
  return data;
};
