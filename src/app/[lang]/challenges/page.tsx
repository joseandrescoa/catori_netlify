import UnderConstruction from "@/components/underConstruction/component";

export default async function Home({ params }: { params: { lang: string } }) {
  return <UnderConstruction />;
}
