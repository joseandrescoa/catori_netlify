"use client";
import Navbar from "@/components/navbar/component";
import { useEffect, useState } from "react";
import { getRegisterForms } from "./requests";

type RegisterFields = {
  placeholder: string;
  field: string;
  type: string;
  label: string;
  required: boolean;
  columnSize: number;
  order: number;
};

type HomePage = {
  params: { lang: string };
  children: React.ReactNode;
};
export default function LangLayout({ children, params }: HomePage) {
  const [registerFields, setRegisterFields] = useState<RegisterFields>(
    [] as any
  );

  useEffect(() => {
    async function fetchRegisterForms() {
      const data = await getRegisterForms(params.lang);
      setRegisterFields(
        data.data
          ?.sort((a: any, b: any) => a.attributes.order - b.attributes.order)
          .map((x: any) => x.attributes)
      );
    }

    fetchRegisterForms();
  }, [params.lang]);
  return (
    <div className="min-h-screen flex flex-col py-32 sm:py-0">
      <div className="flex-1 flex bg-black flex-col sm:flex-row">
        <Navbar
          registerFields={registerFields}
          lang={params.lang}
          className="order-first sm:w-40 hidden sm:block"
        />
        <main className="flex-1 bg-[#0e0e0e] pl-6 pr-6 pt-[70px]">
          {children}
        </main>
      </div>
    </div>
  );
}
