"use client";

import { useEffect, useState } from "react";
import CasinoComponent from "@/components/casinopage/component";

type CasinoGame = {
  title: string;
  provider: string;
  bg_image: any;
};

type CasinoGames = {
  all: CasinoGame[];
  filtered: CasinoGame[];
};

export default function Casino() {
  const [searchTerm, setSearchTerm] = useState<string>("");

  const [casinoGames, setCasinoGames] = useState<CasinoGames>({
    all: [],
    filtered: [],
  });

    return (
        <div className="flex flex-col items-center">
            <CasinoComponent
                searchTerm={searchTerm}
                setSearchTerm={setSearchTerm}
            >
            </CasinoComponent>
        </div>
    );

}
