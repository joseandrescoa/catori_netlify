"use client";

import HomeComponent from "@/components/homepage/component";
import { useEffect, useState } from "react";
import { getPromotionData, getRegisterForms } from "./requests";

type CasinoGame = {
  title: string;
  provider: string;
  bg_image: any;
};

type CasinoGames = {
  all: CasinoGame[];
  filtered: CasinoGame[];
};

type HomePage = {
  params: { lang?: string };
};

export default function Home({ params }: HomePage) {
  const [searchTerm, setSearchTerm] = useState<string>("");

  const [casinoGames, setCasinoGames] = useState<CasinoGames>({
    all: [],
    filtered: [],
  });
  const [promoData, setPromoData] = useState([]);

  // useEffect(() => {
  //   async function fetchCasinoGames() {
  //     const data = await getCasinoGames(params.lang, searchTerm);
  //     setCasinoGames({
  //       all: data.all,
  //       filtered: data.filtered,
  //     });
  //   }
  //
  //   async function fetchPromoData() {
  //     const data = await getPromotionData(params.lang);
  //     setPromoData(data);
  //   }
  //
  //   fetchCasinoGames();
  //   fetchPromoData();
  // }, [params.lang, searchTerm]);

  return (
    <div className="flex flex-col items-center">
      <HomeComponent
        promoData={promoData}
        casinoGames={casinoGames}
        searchTerm={searchTerm}
        setSearchTerm={setSearchTerm}
      />
    </div>
  );
}
