import Navbar from "@/components/navbar/component";

export default async function LangLayout({
  children,
  params,
}: {
  children: React.ReactNode;
  params: {
    lang: string;
  };
}) {
  return <main className="flex-1">{children}</main>;
}
