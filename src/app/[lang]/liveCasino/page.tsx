"use client";

import { useEffect, useState } from "react";
import LiveCasinoComponent from "@/components/liveCasinoPage/component";

type LiveCasinoGame = {
    title: string;
    provider: string;
    bg_image: any;
};

type LiveCasinoGames = {
    all: LiveCasinoGame[];
    filtered: LiveCasinoGame[];
};

export default function LiveCasino() {
    const [searchTerm, setSearchTerm] = useState<string>("");

    const [liveCasinoGames, setLiveCasinoGames] = useState<LiveCasinoGames>({
        all: [],
        filtered: [],
    });

    return (
        <div className="flex flex-col items-center">
            <LiveCasinoComponent />
        </div>
    );

}
