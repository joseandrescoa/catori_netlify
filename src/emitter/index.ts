import mitt, { Emitter } from "mitt";

export type ModalType =
  | "registration"
  | "login"
  | "deposit"
  | "mobile-menu"
  | "forgotPassword"
  | "after-login"
  | null;

type Events = {
  openModal: ModalType;
};

export const closeModal = () => {
  emitter.emit("openModal", null);
};

export const emitter: Emitter<Events> = mitt<Events>();
