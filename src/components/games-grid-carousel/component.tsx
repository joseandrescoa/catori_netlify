import RightChevronIcon from "@/components/icons/rightChevronIcon";
import Card from "@/components/Cards/component";
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
import '@/components/horizontal-swiper/style.css'
import { Navigation, Pagination, Mousewheel, Keyboard } from 'swiper/modules';
import React from "react";

interface Props {
    label: string;
    data: Array<any>;
}

const GamesGridCarousel = ({ label, data }: Props) => {
    const slices = 4;
    const sliceSize = 12 / slices;

    // THE FINAL GRID
    const renderGrid = (slicedData: any) => (
        <div className="grid sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-3 xl:grid-cols-3 gap-3 w-full p-[1rem] bg-[#2d2d2d] rounded-[16px] mb-[2rem]">
            {slicedData.map((game: any, index: any) => (
                <Card
                    key={game.id}
                    title={game.title}
                    image={game.image}
                    additionalClasses={'col-span-1 sm:col-span-1 md:col-span-1 lg:col-span-1 xl:col-span-1'}
                    additionalStyles={{ minHeight: '400px' }}
                />
            ))}
        </div>
    );

    // RENDERING EACH SLIDE
    const renderSlide = (index: any) => {
        const start = index * sliceSize;
        const end = start + sliceSize;
        const slicedData = data.slice(start, end);

        return (
            <SwiperSlide key={index}>{renderGrid(slicedData)}</SwiperSlide>
        );
    };

    return (
        <div className={'w-full pb-[1rem] border-b-[#8080802e] border-b-4'}>
            <div className={'mb-[2rem] flex items-baseline'}>
                <span className={'text-white text-[2.5rem] font-[500]'}>{label}</span>
                <RightChevronIcon />
            </div>
            <Swiper
                cssMode={true}
                navigation={true}
                pagination={true}
                mousewheel={true}
                keyboard={true}
                modules={[Navigation, Pagination, Mousewheel, Keyboard]}
                className={'xl:max-w-[1300px] xl:w-[80vw] lg:w-[80vw] md:w-[60vw] sm:w-[50vw] w-[90vw]'}
            >
                {Array.from({ length: slices }).map((_, index) => renderSlide(index))}
            </Swiper>
        </div>
    );
};

export default GamesGridCarousel;
