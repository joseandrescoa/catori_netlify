import React from "react";

const RegisterFooter = ({ onClick }: any) => {
  return (
    <div className="flex items-center justify-center p-6 rounded-b">
      <button
        className="bg-emerald-500 w-full text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
        type="button"
        onClick={onClick}
      >
        Register
      </button>
    </div>
  );
};

export default RegisterFooter;
