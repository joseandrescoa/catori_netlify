import RightChevronIcon from "@/components/icons/rightChevronIcon";
import Card from "@/components/Cards/component";
import React from "react";

interface Props {
    label: string;
    data: Array<any>;
    limitedTo?: number;
    extraStyle?: boolean;
    bigCard?: number;
}

const GamesGrid = ({ label, data, limitedTo, extraStyle, bigCard }: Props) => {
    // Slice the data array to get only the first 9 items
    let slicedData = data.slice(0, limitedTo ? limitedTo : data.length);

    return (
        <div className={'w-full pb-[1rem] border-b-[#8080802e] border-b-4 mt-[2rem]'}>
            <div className={'mb-[2rem] flex items-baseline'}>
                <span className={'text-white text-[2.5rem] font-[500] hover:text-red-700'}>{label}</span>
                <RightChevronIcon />
            </div>
            <div className={`
                grid xl:grid-cols-${limitedTo === 9 || limitedTo === 12 ? 6 : 5} lg:grid-cols-${limitedTo === 9 || limitedTo === 12 ? 6 : 4}
                md:grid-cols-${limitedTo === 9 || limitedTo === 12 ? 6 : 4}  
                sm:grid-cols-${limitedTo === 9 || limitedTo === 12 ? 6 : 4} grid-cols-4  gap-4 w-full p-[1rem] bg-[#2d2d2d] rounded-[16px]`
            }>
                {slicedData.map((game, index) => (
                    <Card
                        key={game.id}
                        title={game.title}
                        image={game.image}
                        additionalClasses={extraStyle && index === bigCard ? 'row-span-2 col-span-2' : 'row-span-1'}
                        additionalStyles={extraStyle ? { minHeight: '228px' }: {} }
                    />
                ))}
            </div>
        </div>
    );
};

export default GamesGrid;
