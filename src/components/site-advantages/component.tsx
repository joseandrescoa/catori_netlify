import React from "react";
import money from "@/assets/img/wallet.png";
import casino from "@/assets/img/slot-machine.png";
import bonus from "@/assets/img/bonus.png";

const advantagesData = [
    {
        icon: money.src,
        text: "Your money will arrive in seconds.",
    },
    {
        icon: casino.src,
        text: "Nearly all our users start earning within the first 24 hours.",
    },
    {
        icon: bonus.src,
        text: "Unlock bonuses at every step of your journey.",
    },
];

export default function SiteAdvantages() {
    return (
        <div
            className={
                "flex flex-wrap justify-center w-full lg:gap-[2rem] items-center mt-[2rem] mb-[2rem] items-baseline"
            }
        >
            {advantagesData.map((advantage, index) => (
                <div
                    key={index}
                    className={
                        "flex gap-[1rem] justify-center w-full md:w-[48%] lg:w-[30%] xl:w-[20%] mb-[1rem]"
                    }
                >
                    <img
                        className={"h-[70px] mx-auto opacity-[0.5]"}
                        src={advantage.icon}
                        alt="Description"
                        style={{filter: 'contrast(0.1)'}}
                    />
                    <div className={"w-[2px] h-[70px] bg-[#414141] mx-auto"}></div>
                    <div className={"w-full flex items-center text-center"}>
            <span className={"text-[#414141] text-[0.9rem] leading-[25px] mx-auto"}>
              {advantage.text}
            </span>
                    </div>
                </div>
            ))}
        </div>
    );
}
