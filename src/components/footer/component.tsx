"use client";

import { usePathname } from "next/navigation";
import Link from "next/link";
import { languages } from "@/app/i18n/settings";

export default function Footer({
  lang,
  bottom = false,
}: {
  lang: string;
  bottom?: boolean;
}) {
  const pathname = usePathname();
  let urlSuffix = pathname.split(lang)[1];

  return (
    <footer className={`${bottom ? "absolute bottom-2" : ""}`}>
      {languages
        .filter((l) => lang !== l)
        .map((l, index) => {
          return (
            <span key={l}>
              {index > 0 && " or "}
              <Link href={`/${l}` + urlSuffix ?? null}>
                {l === "pt" ? "Portuguese" : "English"}
              </Link>
            </span>
          );
        })}
    </footer>
  );
}
