import React from "react";
import MainButton from "../mainButton/component";

const UnderConstruction = () => {
  return (
    <div className="bg-secondary-tertiary w-full h-screen overflow-hidden flex items-center justify-center flex-col gap-5">
      <h1 className="text-4xl text-white font-bold">
        This Page Is Under Construction
      </h1>
      <MainButton title={"Homepage"} link="/" type="link" />
    </div>
  );
};

export default UnderConstruction;
