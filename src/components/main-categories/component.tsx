import React from "react";
import Category_1 from "@/assets/img/category-1.png";
import Category_2 from "@/assets/img/category-3.png";
import Category_3 from "@/assets/img/category-6.png";
import Category_4 from "@/assets/img/category-8.png";
import Link from "next/link";

const advantagesData = [
    {
        icon: Category_1.src,
        text: "Casino",
        href: "/casino"
    },
    {
        icon: Category_2.src,
        text: "Live Casino",
        href: "/liveCasino"

    },
    {
        icon: Category_3.src,
        text: "Promotions",
        href: "/"

    },
    {
        icon: Category_4.src,
        text: "Sports",
        href: "/sports"

    },
];

const MainCategories = () => {
    return (
        <div
            className={
                "flex flex-wrap justify-center w-full lg:gap-[2rem] gap-[1rem] md:gap-[1rem] items-center mt-[2rem] mb-[2rem]"
            }
        >
            {advantagesData.map((advantage, index) => (
                <Link
                    key={index}
                    href={advantage.href}
                    className={
                        "flex flex-col gap-[1rem] cursor-pointer justify-center p-4 bg-[#b91c1c] border-4 border-[#ef4444] rounded-[15px] transform transition-transform hover:scale-110 duration-300 ease-in-out w-full md:w-[28%] lg:w-[20%] xl:w-[20%] mb-[1rem]"
                    }
                >
                    <img
                        className={"h-[70px] mt-[-45px] mx-auto"}
                        src={advantage.icon}
                        alt="Description"
                    />
                    <div className={"w-full flex justify-center"}>
            <span className={"text-white text-[1.5rem] leading-[25px]"}>
              {advantage.text}
            </span>
                    </div>
                </Link>
            ))}
        </div>
    );
};

export default MainCategories;
