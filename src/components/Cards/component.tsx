import React, { useState } from 'react';
import Link from 'next/link';
import Fire from "@/assets/img/fire.png";

interface Props {
    key: string;
    title: string;
    image: string;
    additionalClasses?: string;
    additionalStyles?: React.CSSProperties;
}

const Card = ({ key, title, image, additionalClasses, additionalStyles }: Props) => {
    const [isHover, setHover] = useState(false);

    const playIcon = (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-24 w-24 xl:h-16 xl:w-16 hover:stroke-red-500 cursor-pointer"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            strokeWidth={2}
        >
            <path strokeLinecap="round" strokeLinejoin="round" d="M14.752 11.168l-3.197-2.132A1 1 0 0010 9.87v4.263a1 1 0 001.555.832l3.197-2.132a1 1 0 000-1.664z" />
            <path strokeLinecap="round" strokeLinejoin="round" d="M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
    );

    return (
        <Link
            href="#"
            key={key}
            className={`relative flex-1 rounded-md flex flex-col justify-between min-h-[228px] p-4 ${additionalClasses}`}
            style={{
                background: `url(${image})`,
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center',
                height: '100%',
                boxShadow:'0px 5px 5px -3px rgba(0, 0, 0, 0.2), 0px 8px 10px 1px rgba(0, 0, 0, 0.14), 0px 3px 14px 2px rgba(0, 0, 0, 0.12)',
                ...additionalStyles,
            }}
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
        >
            <div className="overlay flex justify-end">
                <img className={'w-[30px] h-[30px] bg-black p-[5px] rounded-[50%]'} src={Fire.src} alt={'fire'} />
            </div>
            {isHover && (
                <div className="absolute z-10 w-full h-full top-0 left-0 bg-black bg-opacity-70 transition-opacity duration-100">
                    <div className="text-[1rem] text-center font-bold mt-1">{'Click to Play'}</div>
                    <div className="absolute top-1/2 left-1/2 -translate-y-1/2 -translate-x-1/2">{playIcon}</div>
                </div>
            )}
        </Link>
    );
};

export default Card;

