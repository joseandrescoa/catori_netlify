import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
import '@/components/horizontal-swiper/style.css';
import { Navigation, Pagination, Mousewheel, Keyboard } from 'swiper/modules';
import BaseButton from "@/components/base-button/component";

interface Props {
    images: any;
    type?: string;
}

const msgClass = 'bg-[#00000024] text-center rounded-[15px]';

const HorizontalSwiper = ({ images, type }: Props) => {
    return (
        <div id={'horizontal-swiper'} className={'w-full'}>
            <Swiper
                cssMode={true}
                navigation={true}
                pagination={true}
                mousewheel={true}
                keyboard={true}
                modules={[Navigation, Pagination, Mousewheel, Keyboard]}
                className="mySwiper"
            >
                {images.map((image: any, index: any) => (
                    <SwiperSlide key={index}>
                        <div
                            className={`rounded-[10px] ${type === 'xs' ? 'h-[240px]' : 'h-[500px]'}`}
                            style={{
                                backgroundImage:`url(${image.src})`,
                                backgroundSize: 'cover',
                                backgroundPosition: 'center'
                            }}
                        >
                            {type === 'xs' ?
                                <div className={'flex h-full font-[700] backdrop-blur-[3px] text-white w-full justify-center items-center flex-col leading-normal'}>
                                    <span className={`text-[1.5rem] ${msgClass} `}>{image?.title}</span>
                                    <span className={`text-[1.2rem] ${msgClass}`}>{image?.sub_title}</span>
                                    <span className={`text-[1rem] ${msgClass}`}>{image?.extra_info}</span>
                                    <BaseButton
                                        className="w-[200px] mx-auto md:ml-auto mt-5"
                                        title="Claim"
                                        color="secondary"
                                        onClick={() => { }}
                                    />
                                </div>
                                :
                                <div className={'flex h-full font-[700] backdrop-blur-[3px] text-white w-full justify-center items-center flex-col leading-normal'}>
                                    <span className={`text-[3.5rem] ${msgClass} `}>{image?.title}</span>
                                    <span className={`text-[2.5rem] ${msgClass}`}>{image?.sub_title}</span>
                                    <span className={`text-[1.5rem] ${msgClass}`}>{image?.extra_info}</span>
                                    <BaseButton
                                        className="w-[200px] mx-auto md:ml-auto mt-5"
                                        title="Claim"
                                        color="secondary"
                                        onClick={() => { }}
                                    />
                                </div>
                            }

                        </div>
                    </SwiperSlide>
                ))}
            </Swiper>
        </div>
    );
};

export default HorizontalSwiper;
