import Link from "next/link";
import React from "react";
import MainButton from "../mainButton/component";
import Modal from "../modal/component";
import FormGenerator from "../formGenerator/component";

const Header = ({
  isOpenMenu,
  registerFields,
}: {
  isOpenMenu: boolean;
  registerFields: any;
}) => {
  const [showModal, setShowModal] = React.useState(false);
  const [authData, setAuthData] = React.useState(null);

  const openModal = () => {
    setShowModal(true);
  };
  return (
    <div
      className={`absolute transition-in-out top-0 right-0  h-[53px] shadow-xl bg-black text-black px-[60px] flex justify-between items-center ${
        isOpenMenu ? "w-[calc(100%-232px)]" : "w-[calc(100%-60px)]"
      }`}
    >
      <div className="w-full max-w-[1300px] flex items-center justify-end mx-auto pl-[20px]">
        <div className="flex items-center gap-3">
          <MainButton
            title="Login"
            type="empty"
            onClick={() => alert("Login")}
          />
          <MainButton title="Register" type="filled" onClick={openModal} />
        </div>
      </div>

      <Modal
        showModal={showModal}
        setShowModal={setShowModal}
        title="Create an Account"
        content={
          <FormGenerator
            fields={registerFields}
            authData={authData}
            setAuthData={setAuthData}
            formType="auth"
          />
        }
        footerType="register"
        onClick={() => console.log(authData)}
      />
    </div>
  );
};

export default Header;
