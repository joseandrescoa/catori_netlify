import React from "react";
import HorizontalBanner from "@/components/horizontal-banner/content";
import {dummyData} from "@/components/casinopage/data";
import GamesGrid from "@/components/games-grid/comoponent";
import Categories from "@/components/categories/component";
import Promotions from "@/components/promotions/component";

import Category_1 from "@/assets/img/category-12.webp";
import Category_2 from "@/assets/img/category-9.webp";
import Category_3 from "@/assets/img/category-10.webp";
import Category_4 from "@/assets/img/category-11.webp";
import Category_5 from "@/assets/img/category-13.png";

const advantagesData = [
    {
        icon: Category_1.src,
        text: "Popular",
    },
    {
        icon: Category_2.src,
        text: "New",
    },
    {
        icon: Category_3.src,
        text: "Megaways",
    },
    {
        icon: Category_5.src,
        text: "Jackspot",
    },
    {
        icon: Category_4.src,
        text: "Virtual Sports",
    },
];

const CasinoComponent = ({
       promoData,
   }: any) => {
    return (
        <div className="max-w-[1300px] pl-[2rem] pr-[2rem] flex items-center flex-col">
            <Promotions promoData={'test'} />
            <Categories categories={advantagesData}/>
            <HorizontalBanner />
            {/*<SearchBar></SearchBar>*/}
            <GamesGrid label={'Popular'} data={dummyData} limitedTo={12}></GamesGrid>
            <HorizontalBanner />
            <GamesGrid label={'New'} data={dummyData} limitedTo={12}></GamesGrid>
            <HorizontalBanner />
            <GamesGrid label={'Megaways'} data={dummyData} limitedTo={12}></GamesGrid>
            <HorizontalBanner />
            <GamesGrid label={'Jackpots'} data={dummyData} limitedTo={12}></GamesGrid>
            <HorizontalBanner />
            <GamesGrid label={'Virtual Sports'} data={dummyData} limitedTo={12}></GamesGrid>
        </div>
    );
};

export default CasinoComponent;
