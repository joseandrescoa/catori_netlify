import React from "react";
import TimesIcon from "../icons/timesIcon";
import RegisterFooter from "../footerContent/registerFooter";

type ModalProps = {
  showModal: boolean;
  setShowModal: Function;
  title: string;
  content: any;
  footerType: string;
  onClick: Function;
};

export default function Modal({
  showModal,
  setShowModal,
  title,
  content,
  footerType,
  onClick,
}: ModalProps) {
  React.useEffect(() => {
    function handleKeyPress(event: any) {
      if (event.keyCode === 27) {
        setShowModal(false);
      }
    }

    document.addEventListener("keydown", handleKeyPress);

    return () => {
      document.removeEventListener("keydown", handleKeyPress);
    };
  }, []);

  const openModal = () => {
    setShowModal(true);
  };

  const closeModal = () => {
    setShowModal(false);
  };

  const footerGenerator = () => {
    switch (footerType) {
      case "register":
        return <RegisterFooter onClick={onClick} />;
    }
  };

  return showModal ? (
    <>
      <div className="justify-center items-center flex overflow-x-hidden bg-[#1a2c3863] overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
        <div className="relative w-full my-6 mx-auto max-w-[500px]">
          {/*content*/}
          <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-secondary-tertiary outline-none focus:outline-none">
            {/*header*/}
            <div className="flex items-start justify-between py-4 px-3 rounded-t">
              <h3 className="text-xl font-semibold text-white text-center w-full">
                {title}
              </h3>
              <button
                className="p-1 ml-auto bg-transparent border-0 opacity-1 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                onClick={closeModal}
              >
                <TimesIcon width={18} height={18} className="" />
              </button>
            </div>
            {/*body*/}
            <div className="my-3">{content}</div>

            {/*footer*/}
            {footerGenerator()}
          </div>
        </div>
      </div>
      <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
    </>
  ) : null;
}
