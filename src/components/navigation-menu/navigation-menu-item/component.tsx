"use client";

import React from "react";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";

export default function NavigationMenuItem({
  name,
  icon,
  href,
  className,
  isOpenMenu,
}: {
  name: string;
  icon: string;
  href: string;
  className?: string;
  isOpenMenu: boolean;
}) {
  const pathName = usePathname();
  const width = 30;
  const height = 30;
  return (
    <Link
      href={href}
      className={` w-full flex  items-center ${className} hover:bg-secondary-tertiary rounded-2xl ${
        isOpenMenu
          ? "py-3"
          : "p-2 max-w-fit mb-3 group relative overflow-visible"
      } ${pathName?.includes(href) ? "text-white bg-secondary-tertiary" : ""}`}
    >
      <img
        className={`${isOpenMenu ? "ml-3 mr-3" : ""}`}
        src={icon}
        alt={`icon-${name}`}
        width={width}
        height={height}
        style={{
          width,
          height,
        }}
      />
      {isOpenMenu ? (
        <span className="text-base font-bold"> {name} </span>
      ) : (
        <span className="absolute bottom-[100%] scale-0 w-max transition-all rounded bg-gray-400 p-2 text-xs text-white group-hover:scale-100">
          {name}
        </span>
      )}
    </Link>
  );
}
