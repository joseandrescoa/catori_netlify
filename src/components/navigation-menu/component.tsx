import React from "react";
import NavigationMenuItem from "./navigation-menu-item/component";
import { NavigationRoute } from "@/types";

const getNavigationMenuItems = (
  items: NavigationRoute[],
  lang: string,
  isOpenMenu: boolean
) =>
  items.map((item) => (
    <NavigationMenuItem
      key={item.href}
      name={item.name}
      href={item.href}
      icon={item.icon}
      isOpenMenu={isOpenMenu}
    />
  ));

export default function NavigationMenu({
  navigationList,
  lang,
  isOpenMenu,
}: {
  navigationList: NavigationRoute[];
  className?: string;
  lang: string;
  isOpenMenu: boolean;
}) {
  return (
    <nav
      className={`flex flex-col w-full items-center ${
        isOpenMenu ? "p-4" : "p-2"
      }`}
    >
      {getNavigationMenuItems(navigationList, lang, isOpenMenu)}
    </nav>
  );
}
