import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
import '@/components/vertical-swiper/style.css';
import { Pagination } from 'swiper/modules';

interface Props {
    images: Array<any>;
}

const VerticalSwiper = ({ images }: Props) => {
    return (
        <div id={'vertical-swiper'} className={'w-full'}>
            <Swiper
                direction={'vertical'}
                pagination={{
                    clickable: true,
                }}
                modules={[Pagination]}
            >
                {images.map((image, index) => (
                    <SwiperSlide key={index}>
                        <img className={'rounded-[10px] h-full '} src={image.src} alt={`Description ${index}`} />
                    </SwiperSlide>
                ))}
            </Swiper>
        </div>
    );
};

export default VerticalSwiper;
