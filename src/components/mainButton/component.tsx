import React from "react";
import Link from "next/link";

const MainButton = ({
  title,
  link,
  type,
  onClick = () => {},
}: {
  title: string;
  link?: any;
  type?: string;
  onClick?: any;
}) => {
  return type === "link" ? (
    <Link
      href={link}
      className="rounded-xl bg-tertiary p-3 hover:bg-secondary-tertiary text-white border-0"
    >
      {title}
    </Link>
  ) : type === "filled" ? (
    <button
      onClick={onClick}
      className="rounded-xl bg-cyan-500 px-3 py-2 text-sm hover:bg-cyan-400 text-white border-0 font-semibold"
    >
      {title}
    </button>
  ) : (
    <button
      onClick={onClick}
      className="rounded-xl  px-3 py-2 text-sm text-white border-0 font-bold hover:bg-secondary-hover"
    >
      {title}
    </button>
  );
};

export default MainButton;
