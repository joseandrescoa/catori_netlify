import LoadingGif from "@/components/icons/loadingGif";
import React, { useState, useEffect } from 'react';
import HorizontalSwiper from "@/components/horizontal-swiper/component";
import photo_1 from '@/assets/img/slide-9.webp';
import photo_4 from '@/assets/img/slide-4.jpeg';
import photo_5 from '@/assets/img/slide-5.jpeg';
import photo_6 from '@/assets/img/slide-11.webp';



const images = [
    {
        src: photo_6.src,
        title: 'WELCOME BONUS',
        sub_title: '15% Cashback',
        extra_info: 'UP TO $500'
    },
    {
        src: photo_1.src,
        title: 'RELOAD BONUS',
        sub_title: '10% more',
        extra_info: 'On every reload'
    },
];

const images_v2 = [
    {
        src: photo_4.src,
        title: 'HIGH ROLLER BONUS',
        sub_title: 'Exclusive perks for high rollers',
    },
    {
        src: photo_5.src,
        title: 'BIRTHDAY BONUS',
        sub_title: 'Special bonus on your birthday',
    },
];

const images_v3 = [
    {
        src: photo_5.src,
        title: 'TOURNAMENT BONUSES',
        sub_title: 'Compete for prizes',
    },
    {
        src: photo_4.src,
        title: 'VIP PROGRAM',
        sub_title: 'Exclusive benefits',
    },
];


export default function Promotions(promoData: any) {
    const { data } = promoData.promoData;

    const [screenDimensions, setScreenDimensions] = useState({
        width: typeof window !== 'undefined' ? window.innerWidth : 0,
        height: typeof window !== 'undefined' ? window.innerHeight : 0,
    });

    useEffect(() => {
        const updateDimensions = () => {
            setScreenDimensions({
                width: window.innerWidth,
                height: window.innerHeight,
            });
        };

        window.addEventListener("resize", updateDimensions);

        return () => {
            window.removeEventListener("resize", updateDimensions);
        };
    }, []); // Ejecuta el efecto solo una vez al montar el componente

    const generateImageUrl = (item: any) => {
        return item?.attributes?.bg_image?.data?.attributes?.formats?.thumbnail?.url;
    };

    const isMobile = screenDimensions.width <= 600;

    return (
        <div className={`flex items-start justify-center gap-[1rem] max-w-[1300px] w-${isMobile ? '[90vw]' : '[70vw]'} sm:w-[70vw] md:w-[80vw] p-2`}>
            <div className={`lg:w-[67%] w-[95%] ${isMobile ? 'md:w-[90vw]' : ''}`}>
                <HorizontalSwiper images={images} />
            </div>
            {!isMobile && (
                <div className={'lg:w-[32%] w-0'}>
                    <div className={'flex flex-col gap-[1rem]'}>
                        <HorizontalSwiper images={images_v2} type={'xs'}></HorizontalSwiper>
                        <HorizontalSwiper images={images_v3} type={'xs'}></HorizontalSwiper>
                    </div>
                </div>
            )}
        </div>
    );
}
