export default function RightChevronIcon ({ className }: { className?: string }) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" className={`h-6 w-6 transition-all duration-100 transform stroke-[white] hover:stroke-red-700 ${className}`} fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
      <path strokeLinecap="round" strokeLinejoin="round" d="M9 5l7 7-7 7" />
    </svg>
  );
}