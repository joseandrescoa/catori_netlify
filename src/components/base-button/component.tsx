import React from 'react';
import Link from 'next/link';
import style from './style.module.css';

interface BaseButtonProps {
  href?: any;
  color: 'primary' | 'secondary' | 'accent';
  title: string;
  className?: string;
  onClick?: () => void;
  type?: 'button' | 'submit' | 'reset';
  isLoading?: boolean;
  disabled?: boolean;
}

export default function BaseButton(
    {
        href,
        color,
        title,
        className,
        onClick,
        type = 'button',
        isLoading = false,
        disabled = false,
    }: BaseButtonProps) {
    // Ahora buttonStyle puede tener width y border opcionalmente
  const buttonStyle: { width?: string; border?: string } =
        title === 'NEXT' || title === 'Previous' || title === 'SUBMIT'
            ? { width: '100%' }
            : {};
  // Agregar el estilo condicional para el borde aquí
  if (disabled || isLoading) {
    buttonStyle.border = 'none';
  }

  if (href) {
    return (
        <Link href={href} className={`${style[color]} ${style.baseButton} ${className}`}>
          <span>{title}</span>
        </Link>
    );
  }

  if (onClick) {
    return (
        <button
            disabled={isLoading || disabled}
            style={buttonStyle}
            type={type}
            className={`${style[color]} ${style.baseButton} ${className} flex items-center justify-center`}
            onClick={onClick}
        >
          <span>{title}</span>
          {isLoading && (
              <div
                  className="ml-2 inline-block h-4 w-4 animate-spin rounded-full border-4 border-solid border-current border-r-transparent align-[-0.125em] motion-reduce:animate-[spin_1.5s_linear_infinite]"
                  role="status"
              >
            <span className="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]">
              Loading...
            </span>
              </div>
          )}
        </button>
    );
  } else return <div>Error</div>;
}
