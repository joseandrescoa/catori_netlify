import React from "react";
import Link from "next/link";
import LoadingGif from "../icons/loadingGif";

const SearchContent = ({ casinoGames, searchTerm }: any) => {
  // const { data } = casinoGames.filtered;

  const generateImageUrl = (item: any) => {
    return item?.attributes?.bg_image?.data?.attributes?.formats?.thumbnail
      ?.url;
  };

  return (
    <div className="absolute top-full z-[999] bg-tertiary mt-[-10px] rounded-md p-4 w-full text-sm text-center flex gap-2">
      {/*{searchTerm.length >= 3 ? (*/}
      {/*  !data ? (*/}
      {/*    <div className="w-fit mx-auto my-3">*/}
      {/*      <LoadingGif />*/}
      {/*    </div>*/}
      {/*  ) : data?.length ? (*/}
      {/*    data?.map((promo: any, i: number) => (*/}
      {/*      <Link*/}
      {/*        key={i}*/}
      {/*        href="#"*/}
      {/*        className={`flex-1 max-w-[175px] rounded-md flex flex-col justify-between  min-h-[228px]  p-4`}*/}
      {/*        style={{*/}
      {/*          background: `url(${*/}
      {/*            process.env.NEXT_PUBLIC_STRAPI_URL_MEDIA +*/}
      {/*            generateImageUrl(promo)*/}
      {/*          })`,*/}
      {/*          backgroundSize: "cover",*/}
      {/*          backgroundRepeat: "no-repeat",*/}
      {/*        }}*/}
      {/*      ></Link>*/}
      {/*    ))*/}
      {/*  ) : (*/}
      {/*    <span className="text-center">No results found.</span>*/}
      {/*  )*/}
      {/*) : (*/}
      {/*  <span className="text-center">*/}
      {/*    Search requires at least 3 characters.*/}
      {/*  </span>*/}
      {/*)}*/}
    </div>
  );
};

export default SearchContent;
