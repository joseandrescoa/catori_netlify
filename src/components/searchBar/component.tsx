"use client";

import React, { useEffect, useRef, useState } from "react";
import SearchIcon from "../icons/searchIcon";
import SearchContent from "./SearchContent";
import TimesIcon from "../icons/timesIcon";
import Input from "../formGenerator/fieldTypes/Input";

const SearchBar = ({ casinoGames, searchTerm, setSearchTerm }: any) => {
  const [showSearchContent, setShowSearchContent] = useState(false);
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (ref.current && !ref.current?.contains(event.target)) {
        setShowSearchContent(false);
        setSearchTerm("");
      }
    };

    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);
  return (
    <div className="w-full">
      {showSearchContent && (
        <div className="w-full h-full absolute top-0 left-0 bg-[#1a2e38b3]"></div>
      )}
      <div className="w-full relative">
        <div ref={ref} className="flex flex-col items-center gap-1">
          <div className="my-5 w-full px-3 rounded-full border-2 border-secondary-hover bg-secondary-tertiary text-white flex items-center gap-3">
            <SearchIcon width={15} height={15} />
            <Input
              label="search"
              placeholder="Search..."
              type="text"
              value={searchTerm}
              showLabel={false}
              onFocus={() => setShowSearchContent(true)}
              onChange={(e: any) => setSearchTerm(e.target.value)}
            />
            {showSearchContent && (
              <div
                onClick={() => {
                  setShowSearchContent(false);
                  setSearchTerm("");
                }}
                className="cursor-pointer w-fit"
              >
                <TimesIcon width={20} height={20} />
              </div>
            )}
          </div>
          {showSearchContent && (
            <SearchContent casinoGames={casinoGames} searchTerm={searchTerm} />
          )}
        </div>
      </div>
    </div>
  );
};

export default SearchBar;
