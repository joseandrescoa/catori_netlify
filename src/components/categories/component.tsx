import React from "react";

interface Props {
    categories: any;
}

const Categories = ({ categories }:Props) => {
    return (
        <div className={'flex justify-center gap-[3rem] w-[95%] lg:gap-[1rem] items-center mt-[4rem] flex-wrap mb-[2rem]'}>
            {categories?.map((advantage: any, index: any) => (
                <div
                    key={index}
                    className={'flex mt-[1rem] flex-col items-center gap-[0.5rem] cursor-pointer justify-center p-[0.5rem] bg-[#b91c1c] border-4 border-[#ef4444] rounded-[15px] transform transition-transform hover:scale-110 duration-300 ease-in-out w-full md:w-[40%] lg:w-[18%] xl:w-[18%] mb-[1rem]'}
                >
                    <img className={'h-[50px] w-[50px] mt-[-45px]'} src={advantage.icon} alt="Description" />
                    <div className={'w-full flex justify-center'}>
                        <span className={'text-white text-[1rem] leading-[25px]'}>{advantage.text}</span>
                    </div>
                </div>
            ))}
        </div>
    );
}

export default Categories;
