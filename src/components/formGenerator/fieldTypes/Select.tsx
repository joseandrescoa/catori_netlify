import { months } from "@/components/generalData";
import React from "react";
import Select from "react-select";

const MainSelect = ({
  placeholder,
  label,
  onChange,
  showLabel,
  value,
  required,
}: {
  placeholder: string;
  label?: string;
  showLabel: boolean;
  onChange: any;
  value?: string;
  required?: boolean;
}) => {
  const [options, setOptions] = React.useState<any>([]);

  React.useEffect(() => {
    if (label === "month") {
      setOptions(months);
    }
  }, []);

  return (
    <div className={`relative w-full`}>
      <Select
        id={label}
        isSearchable
        isClearable
        options={options}
        name={label}
        classNames={{
          control: () => "bg-transparent p-0 text-sm border-0",
          valueContainer: () => "",
        }}
        placeholder={placeholder}
        onChange={onChange}
        value={value}
      />
      {showLabel ? (
        <label
          htmlFor={label}
          className="absolute left-0 -top-3.5 flex gap-2 text-gray-400 text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-400 peer-placeholder-shown:top-2 peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm"
        >
          {placeholder}
          {required && <span className="text-red-500">*</span>}
        </label>
      ) : null}
    </div>
  );
};

export default MainSelect;
