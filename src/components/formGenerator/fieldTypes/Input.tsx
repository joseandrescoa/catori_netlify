import React from "react";

const Input = ({
  type,
  placeholder,
  label,
  onChange,
  showLabel,
  onFocus,
  value,
  required,
}: {
  type: string;
  placeholder: string;
  label?: string;
  showLabel: boolean;
  onChange: any;
  onFocus?: any;
  value?: string;
  required?: boolean;
}) => {
  const className = `w-full h-10 text-white text-sm  ${
    showLabel ? "border-b-2 placeholder-transparent" : ""
  } border-gray-300 peer focus:outline-none focus:border-gray-600 bg-transparent
  }`;

  return (
    <div className={`relative w-full`}>
      <input
        id={label}
        name={label}
        className={`${className}`}
        type={type}
        placeholder={placeholder}
        onFocus={onFocus}
        onChange={onChange}
        value={value}
      />
      {showLabel ? (
        <label
          htmlFor={label}
          className="absolute left-0 -top-3.5 flex gap-2 text-gray-400 text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-400 peer-placeholder-shown:top-2 peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm"
        >
          {placeholder}
          {required && <span className="text-red-500">*</span>}
        </label>
      ) : null}
    </div>
  );
};

export default Input;
