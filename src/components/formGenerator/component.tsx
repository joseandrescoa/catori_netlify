import React from "react";
import Input from "./fieldTypes/Input";
import MainSelect from "./fieldTypes/Select";

const generateColSize = (column: number) => {
  switch (column) {
    case 1:
      return "8.33%";
    case 2:
      return "16.67%";
    case 3:
      return "25%";
    case 4:
      return "33.33%";
    case 5:
      return "41.67%";
    case 6:
      return "50%";
    case 7:
      return "58.33%";
    case 8:
      return "66.67%";
    case 9:
      return "75%";
    case 10:
      return "83.33%";
    case 11:
      return "91.67%";
    case 12:
      return "100%";
    default:
      return "100%";
  }
};

const fieldGenerator = (item: any, handleInputChange: any) => {
  const { columnSize, field, label, order, placeholder, required, type } = item;

  switch (field) {
    case "input":
      return (
        <div
          className={`grow mb-1`}
          style={{ width: `calc(${generateColSize(columnSize)} - 20px)` }}
        >
          <Input
            label={label}
            placeholder={placeholder}
            required={required}
            type={type}
            showLabel={true}
            onChange={(e: any) => handleInputChange(e)}
          />
        </div>
      );
    case "select":
      return (
        <div
          className={`grow mb-1`}
          style={{ width: `calc(${generateColSize(columnSize)} - 20px)` }}
        >
          <MainSelect
            label={label}
            placeholder={placeholder}
            required={required}
            showLabel={true}
            onChange={(e: any) => handleInputChange(e)}
          />
        </div>
      );
  }
};

const FormGenerator = ({ fields, authData, setAuthData, formType }: any) => {
  const handleInputChange = (e: any) => {
    const { name, value } = e.target;

    if (formType === "auth") {
      setAuthData((prev: any) => ({
        ...prev,
        [name]: value,
      }));
    }
  };

  return (
    <div className="flex flex-wrap items-center px-4 my-3 gap-5">
      {fields?.map((item: any, i: any) =>
        fieldGenerator(item, handleInputChange)
      )}
    </div>
  );
};

export default FormGenerator;
