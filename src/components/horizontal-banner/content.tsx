import React from "react";
import banner from "@/assets/img/slide-10.webp";
import Link from "next/link";


const HorizontalBanner = () => {

    return (
        <div className={'flex justify-center w-full lg:gap-[3rem] items-center mt-[2rem] mb-[2rem]'}>

            <div className={'flex gap-[1rem] justify-center w-full'}>
                <Link
                    href="#"
                    className={`flex-1 w-full rounded-[16px] flex flex-col justify-between h-[200px] p-4`}
                    style={{
                        background: `url(${banner.src})`,
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat",
                        backgroundPosition: 'center'
                    }}
                >
                </Link>
            </div>

        </div>
    );
}
export default HorizontalBanner;