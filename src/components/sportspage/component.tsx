"use client";

import React from "react";
import Photo from "@/assets/img/sportbook.webp"
import Promotions from "@/components/promotions/component";


const SportsComponent = ({
 promoData,
 }: any) => {
    return (
        <div className="max-w-[1300px] pl-[2rem] pr-[2rem] gap-[2rem] flex items-center flex-col">
                <Promotions promoData={'test'} />
                <img src={Photo.src}/>
        </div>
    );
};

export default SportsComponent;
