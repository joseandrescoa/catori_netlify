import game_1 from "@/assets/img/game-1.webp";
import game_2 from "@/assets/img/game-2.webp";
import game_3 from "@/assets/img/game-3.webp";
import game_4 from "@/assets/img/game-4.jpeg";
import game_5 from "@/assets/img/game-5.png";
import game_6 from "@/assets/img/game-6.jpeg";
import game_7 from "@/assets/img/game-7.jpeg";
import game_8 from "@/assets/img/game-8.jpeg";
import game_9 from "@/assets/img/game-9.jpeg";
import game_10 from "@/assets/img/game-10.jpeg";
import game_11 from "@/assets/img/game-11.webp";
import game_12 from "@/assets/img/game-12.jpeg";
import game_13 from "@/assets/img/game-13.webp";


export const dummyData = [
    { id: '1', title: "Game 1", image: game_1.src },
    { id: '2', title: "Game 2", image: game_2.src },
    { id: '3', title: "Game 3", image: game_4.src },
    { id: '4', title: "Game 4", image: game_3.src },
    { id: '5', title: "Game 5", image: game_5.src },
    { id: '6', title: "Game 6", image: game_6.src },
    { id: '7', title: "Game 7", image: game_7.src },
    { id: '8', title: "Game 8", image: game_8.src },
    { id: '9', title: "Game 9", image: game_9.src },
    { id: '10', title: "Game 10", image: game_10.src },
    { id: '11', title: "Game 11", image: game_11.src },
    { id: '12', title: "Game 12", image: game_12.src },
    { id: '13', title: "Game 12", image: game_13.src },
];