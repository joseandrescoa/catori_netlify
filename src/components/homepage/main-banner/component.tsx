
import LoadingGif from "@/components/icons/loadingGif";
import Link from "next/link";

export default function MainBanner(promoData: any) {

    // const generateImageUrl = (item: any) => {
    //     return item?.attributes?.bg_image?.data?.attributes?.formats?.thumbnail
    //         ?.url;
    // };

    return (
        <div className={`flex items-start justify-between gap-5 w-[1300px] p-2`}>

                    <Link
                        href="#"
                        className={`flex-1 rounded-md flex flex-col justify-between min-h-[228px] p-4`}
                        style={{
                            background: `url('https://portalbucket.fra1.cdn.digitaloceanspaces.com/assets/home/landing-img.webp)`,
                            backgroundSize: "cover",
                            backgroundRepeat: "no-repeat",
                        }}
                    >
                        <div className="flex flex-col gap-2">
              <span className="bg-white rounded-md py-0 px-1 text-black max-w-fit text-[12px] font-semibold">
                Promotion
              </span>
                            <h3 className="text-xl font-semibold">
                                sadsadasd
                            </h3>
                            <p className="text-sm max-w-[160px]">
                                asdasdadas<strong>Read More</strong>
                            </p>
                        </div>
                        <span className="text-sm border-2 font-semibold border-white rounded-md text-center max-w-fit px-10 py-2 hover:bg-white hover:text-black transition-in-out-2">
             sdfsdfsfsdf
            </span>
                    </Link>
        </div>
    );
}
