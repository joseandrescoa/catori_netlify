"use client";

import Link from "next/link";
import LoadingGif from "../../icons/loadingGif";

export default function GamesByCategories(casinoGames: any) {
  const { data } = casinoGames.casinoGames.all;

  const generateImageUrl = (item: any) => {
    return item?.attributes?.bg_image?.data?.attributes?.formats?.thumbnail
      ?.url;
  };

  return (
    <div
      className={`flex items-start justify-between gap-5 w-full overflow-x-auto p-2 no-scrollbar`}
    >
      {!data ? (
        <div className="w-fit mx-auto my-3">
          <LoadingGif />
        </div>
      ) : (
        data?.map((promo: any, i: number) => (
          <Link
            key={i}
            href="#"
            className={`flex-1 min-w-[175px] rounded-md flex flex-col justify-between min-h-[228px] p-4`}
            style={{
              background: `url(${
                process.env.NEXT_PUBLIC_STRAPI_URL_MEDIA +
                generateImageUrl(promo)
              })`,
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
            }}
          ></Link>
        ))
      )}
    </div>
  );
}
