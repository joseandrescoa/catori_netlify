import React from "react";
import Promotions from "@/components/promotions/component";
import SiteAdvantages from "@/components/site-advantages/component";
import HorizontalBanner from "@/components/horizontal-banner/content";
import {dummyData} from "@/components/homepage/data";
import MainCategories from "@/components/main-categories/component";
import GamesGrid from "@/components/games-grid/comoponent";
import GamesGridCarousel from "@/components/games-grid-carousel/component";

const HomeComponent = ({
       promoData,
   }: any) => {
    return (
        <div className="max-w-[1300px] flex items-center flex-col">

            <Promotions promoData={promoData} />
            <SiteAdvantages></SiteAdvantages>
            <MainCategories></MainCategories>
            <HorizontalBanner></HorizontalBanner>
            <GamesGridCarousel label={'Most Played '} data={dummyData}></GamesGridCarousel>
            <HorizontalBanner></HorizontalBanner>
            <GamesGrid label={'Casino'} data={dummyData} limitedTo={9} extraStyle={true} bigCard={2}></GamesGrid>
            <HorizontalBanner></HorizontalBanner>
            <GamesGrid label={'Casino Live'} data={dummyData} extraStyle={true} bigCard={0}></GamesGrid>
            <HorizontalBanner></HorizontalBanner>
            <GamesGrid label={'Recently Added'} data={dummyData} limitedTo={12}></GamesGrid>

        </div>
    );
};

export default HomeComponent;
