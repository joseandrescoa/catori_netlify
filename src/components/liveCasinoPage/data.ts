import game_1 from "@/assets/img/game-15.jpeg";

export const dummyData = [
    { id: '1', title: "Game 1", image: game_1.src },
    { id: '2', title: "Game 2", image: game_1.src },
    { id: '3', title: "Game 3", image: game_1.src },
    { id: '4', title: "Game 4", image: game_1.src },
    { id: '5', title: "Game 5", image: game_1.src },
    { id: '6', title: "Game 6", image: game_1.src },
    { id: '7', title: "Game 7", image: game_1.src },
    { id: '8', title: "Game 8", image: game_1.src },
    { id: '9', title: "Game 9", image: game_1.src },
    { id: '10', title: "Game 10", image: game_1.src },
    { id: '11', title: "Game 11", image: game_1.src },
    { id: '12', title: "Game 12", image: game_1.src },
    { id: '13', title: "Game 12", image: game_1.src },
];