import React from "react";
import HorizontalBanner from "@/components/horizontal-banner/content";
import {dummyData} from "@/components/liveCasinoPage/data";
import GamesGrid from "@/components/games-grid/comoponent";
import Categories from "@/components/categories/component";
import Promotions from "@/components/promotions/component";
import Category_1 from "@/assets/img/category-3.png";
import Category_2 from "@/assets/img/category-4.png";
import Category_5 from "@/assets/img/category-2.png";
import Category_4 from "@/assets/img/category-1.png";
import Category_6 from "@/assets/img/category-6.png";


const categories = [
    {
        icon: Category_1.src,
        text: 'Live Roulette',
    },
    {
        icon: Category_2.src,
        text: 'Live BlackJack',
    },
    {
        icon: Category_4.src,
        text: 'Live Baccarat',
    },
    {
        icon: Category_5.src,
        text: 'Live Games',
    },
    {
        icon: Category_6.src,
        text: 'VIP Tables',
    }
];

const LiveCasinoComponent = ({
       promoData,
   }: any) => {
    return (
        <div className="max-w-[1300px] pl-[2rem] pr-[2rem] flex items-center flex-col">
            <Promotions promoData={'test'} />
            <Categories categories={categories}/>
            <HorizontalBanner />
            {/*<SearchBar></SearchBar>*/}
            <GamesGrid label={'Live Roulette'} data={dummyData} limitedTo={12}></GamesGrid>
            <HorizontalBanner />
            <GamesGrid label={'Live BlackJack'} data={dummyData} limitedTo={12}></GamesGrid>
            <HorizontalBanner />
            <GamesGrid label={'Game Shows'} data={dummyData} limitedTo={12}></GamesGrid>
            <HorizontalBanner />
            <GamesGrid label={'Live Baccarat'} data={dummyData} limitedTo={12}></GamesGrid>
            <HorizontalBanner />
            <GamesGrid label={'Live Games'} data={dummyData} limitedTo={12}></GamesGrid>
            <HorizontalBanner />
            <GamesGrid label={'VIP Tables'} data={dummyData} limitedTo={12}></GamesGrid>
        </div>
    );
};

export default LiveCasinoComponent;
