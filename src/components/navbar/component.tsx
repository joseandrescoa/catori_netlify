"use client";

import React, { useState } from "react";
import Footer from "@/components/footer/component";
import NavigationMenu from "../navigation-menu/component";
import { navigationMenu } from "./data";

import NavHeader from "./navHeader";
import Header from "../header/component";

export default function Navbar({
  className,
  lang,
  registerFields,
}: {
  className: string;
  lang: string;
  registerFields: any;
}) {
  const [isOpenMenu, setIsOpenMenu] = useState(true);
  const handleMenuSize = () => {
    setIsOpenMenu(!isOpenMenu);
  };
  return (
    <nav
      className={`transition-in-out h-screen ${className} ${
        isOpenMenu ? "lg:w-[232px]" : "lg:w-[60px]"
      }`}
    >
      <Header registerFields={registerFields} isOpenMenu={isOpenMenu} />
      <NavHeader handleMenuSize={handleMenuSize} isOpenMenu={isOpenMenu} />
      <div className="flex flex-col w-full h-[calc(100%-53px)] items-center">
        <NavigationMenu
          lang={lang}
          navigationList={navigationMenu}
          isOpenMenu={isOpenMenu}
        />

        <Footer bottom={true} lang={lang} />
      </div>
    </nav>
  );
}
