import Image from "next/image";
import Link from "next/link";
import logo from "../../assets/img/CatoriTech V3.png"

const NavHeader = ({ handleMenuSize, isOpenMenu }: any) => {
  return (
    <Link
      href={"/"}
      className={`w-full cursor-pointer flex items-center justify-center pt-3 pb-4 gap-2 shadow-xl relative h-[53px]`}
    >
      <div
        className={`group w-full cursor-pointer flex items-center justify-center gap-2  pt-2 pb-2 pr-2 pl-2 rounded-2xl  ${
          isOpenMenu
            ? "py-3"
            : "max-w-fit relative overflow-visible h-fit hover:bg-secondary-tertiary"
        }`}
      >
        <img
          src={logo.src}
          alt={`icon-casino`}
          width={isOpenMenu ? 60 : 0}
          height={isOpenMenu ? 40 : 20}
          style={{
            width: isOpenMenu ? 'auto' : 0,
            height: isOpenMenu ? 40 : 20,
          }}
        />
        {isOpenMenu ? (
          <span className="text-base font-bold"></span>
        ) : (
          <span className="absolute top-full z-[999] scale-0 w-max transition-all rounded bg-gray-400 p-2 text-xs text-white group-hover:scale-100">
            Home
          </span>
        )}
      </div>

      <div
        onClick={handleMenuSize}
        className={`absolute top-[13px] p-[8px] transition-in-out bg-[#2f4553] hover:bg-[#4a6271] shadow-md rounded-[18px] cursor-pointer ${
          isOpenMenu
            ? "right-[-10px]"
            : "rotate-180 right-[-15px] top-[14px] p-[6px]"
        } `}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="16"
          height="16"
          fill="#fff"
          viewBox="0 0 16 16"
        >
          <path
            fill-rule="evenodd"
            d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"
          />{" "}
        </svg>
      </div>
    </Link>
  );
};

export default NavHeader;
