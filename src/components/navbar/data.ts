import { NavigationRoute } from "@/types";
import Category_1 from "@/assets/img/category-1.png";
import Category_2 from "@/assets/img/category-3.png";
import Category_3 from "@/assets/img/category-6.png";
import Category_4 from "@/assets/img/category-8.png";

export const navigationMenu: NavigationRoute[] = [
  {
    icon: Category_2.src,
    name: "Live Casino",
    href: "liveCasino",
  },
  {
    icon: Category_1.src,
    name: "Casino",
    href: "casino",
  },
  {
    icon: Category_4.src,
    name: "Sports",
    href: "sports",
  },
];
