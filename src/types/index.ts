export interface NavigationRoute {
  name: string;
  href: string;
  icon: string;
}
