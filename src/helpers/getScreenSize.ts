export function getScreenSize(setDimensions: Function) {
  let dimensions = {
    width: window.innerWidth,
    height: window.innerHeight,
  };

  const updateDimension = () => {
    setDimensions(dimensions);
  };
  window.addEventListener("resize", updateDimension);

  return () => {
    window.removeEventListener("resize", updateDimension);
  };
}
