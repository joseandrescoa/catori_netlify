/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: true,
  },
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'portalbucket.fra1.cdn.digitaloceanspaces.com',
      }
    ]
  }
}

module.exports = nextConfig
