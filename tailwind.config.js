/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/app/**/*.{js,ts,jsx,tsx}",
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  safelist: [
    {
      pattern: /grid-cols-/,
    },
    {
      pattern: /col-span-/,
    },
  ],
  theme: {
    extend: {
      colors: {
        primary: "#333333",
        secondary: "#fc0f1b",
        "secondary-accent": "#b80303",
        tertiary: "#0f212e",
        "secondary-tertiary": "#1a2c38",
        "secondary-hover": "#4c647436",
        accent: "#1f1f1f",
        "light-gray": "#e6e6e6",
        transparent: "transparent",
      },
    },
  },
  plugins: [],
};
